﻿using UnityEngine;
using System.Collections;

public class rejabrick : MonoBehaviour {

	public float speed= 100f;
	
	void OnCollisionEnter2D(Collision2D  collision) 
	{
		Collider2D collider = collision.collider;
		
		if(collider.tag == "Player")
		{ 
			//Debug.Log(GameConstants.currentPlayerType);
			if (GameConstants.currentPlayerType== Creature.Ghost) 	this.GetComponent<BoxCollider2D>().isTrigger= true;
			else 													this.GetComponent<BoxCollider2D>().isTrigger= false;

			this.GetComponent<BoxCollider2D>().enabled= false;
			this.GetComponent<BoxCollider2D>().enabled= true;
		}

	}

	void OnTriggerStay2D(Collider2D other) 
	{
		if (other.tag== "Player")
		{
			if (GameConstants.currentPlayerType== Creature.Ghost) 	this.GetComponent<BoxCollider2D>().isTrigger= true;
			else 													this.GetComponent<BoxCollider2D>().isTrigger= false;

			this.GetComponent<BoxCollider2D>().enabled= false;
			this.GetComponent<BoxCollider2D>().enabled= true;
		}
	}
	
	void OnTriggerExit2D(Collider2D other) 
	{
		if (other.tag== "Player")
		{
			if (GameConstants.currentPlayerType== Creature.Ghost) 	this.GetComponent<BoxCollider2D>().isTrigger= true;
			else 													this.GetComponent<BoxCollider2D>().isTrigger= false;

			this.GetComponent<BoxCollider2D>().enabled= false;
			this.GetComponent<BoxCollider2D>().enabled= true;
		}
	}


}
