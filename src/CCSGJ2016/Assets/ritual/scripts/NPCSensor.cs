﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class NPCSensor : MonoBehaviour {

	public UnityEvent playerEnters;
	public UnityEvent playerLeaves;

	void OnTriggerStay2D(Collider2D other) {
		if (other.tag== "Player")
		{
			if (playerEnters!=null) playerEnters.Invoke();
		}
	}

	void OnTriggerExit2D(Collider2D other) {
		if (other.tag== "Player")
		{
			if (playerLeaves!=null) playerLeaves.Invoke();
		}
	}
}
