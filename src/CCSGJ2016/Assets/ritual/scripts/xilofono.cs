﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class xilofono : MonoBehaviour {

	public UnityEvent playedFunction;
	public GameObject musicText;

	void OnCollisionEnter2D(Collision2D  collision) 
	{
		Collider2D collider = collision.collider;
		
		if(collider.tag == "Player")
		{ 
			if (GameConstants.currentPlayerType== Creature.Skeleton) {if (playedFunction!=null) playedFunction.Invoke();musicText.SetActive(true);Invoke("HideText",1);}
			else musicText.SetActive(false);

			this.GetComponent<BoxCollider2D>().enabled= false;
			this.GetComponent<BoxCollider2D>().enabled= true;
		}
	}

	void HideText()
	{
		musicText.SetActive(false);
	}
}
