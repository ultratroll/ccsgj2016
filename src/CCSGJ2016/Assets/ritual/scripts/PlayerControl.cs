﻿using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;

public class PlayerControl : MonoBehaviour {

	public float speed= 4f;
	public Creature creatureType;

	public GameObject GhostObject;
	public GameObject SkeletonObject;
	public GameObject ZombieObject;
	public GameObject HumanObject;

	public Transform Checkpoint1;
	public Transform Checkpoint2;
	public Transform Checkpoint3;
	public Transform Checkpoint4;

	public GameObject SpawnText;

	// Use this for initialization
	void Start () {
		
		GameConstants.skeletonUnlocked= true;
		GameConstants.zombieUnlocked= true;
		GameConstants.humanUnlocked= true;
	
		StartCoroutine("FTUX");
	}

	IEnumerator FTUX()
	{
		yield return new WaitForSeconds(1f);

		InventoryUIManager.instance.GodVoice("Al parecer eres un fantasma\n");

		yield return new WaitForSeconds(3.5f);

		InventoryUIManager.instance.GodVoice("Hay una mesa de ritual, traele objetos\n");

		yield return new WaitForSeconds(3.5f);

		InventoryUIManager.instance.GodVoice("Puede que actives un ritual, para renacer\n");

		yield return new WaitForSeconds(3.5f);

		InventoryUIManager.instance.GodVoice("Buena suerte encontrando esas cosas!...\n");

		yield return null;
	}
	
	// Update is called once per frame
	void Update () {
		MovementInput();
		ChangeCharacterInput();
	}

	void MovementInput()
	{
		if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D)) 
		{
			transform.Translate(Vector3.right * speed * Time.deltaTime);
			transform.localScale= new Vector3(1,1,1);
		}
		if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
		{
			transform.Translate(Vector3.left * speed * Time.deltaTime);
			transform.localScale= new Vector3(-1,1,1);
		}

		if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
		{
			transform.Translate(Vector3.up * speed * Time.deltaTime);
		}
		
		if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
		{
			transform.Translate(Vector3.down * speed * Time.deltaTime);
		}
	}

	void ChangeCharacterInput()
	{
		if (Input.GetKeyDown(KeyCode.Q)) 
		{
			switch(this.creatureType)
			{
			case Creature.Ghost:
				if (GameConstants.skeletonUnlocked) ChangeCharacter(Creature.Skeleton);
				break;
			case Creature.Skeleton:
				if (GameConstants.zombieUnlocked) ChangeCharacter(Creature.Zombie);
				else 				ChangeCharacter(Creature.Ghost);
				break;
			case Creature.Zombie:
				if (GameConstants.humanUnlocked) ChangeCharacter(Creature.Human);
				else 				ChangeCharacter(Creature.Ghost);
				break;
			case Creature.Human:
				ChangeCharacter(Creature.Ghost);
				break;
			}

		}
	}

	public void ChangeCharacter(Creature creatureType)
	{
		GameConstants.currentPlayerType= creatureType;
		this.creatureType= creatureType;

		this.GhostObject.SetActive(false);
		this.SkeletonObject.SetActive(false);
		this.ZombieObject.SetActive(false);
		this.HumanObject.SetActive(false);

		MasterAudio.PlaySoundAndForget("transformacion");

		switch(this.creatureType)
		{
			case Creature.Ghost:
				this.GhostObject.SetActive(true);
			break;
			case Creature.Skeleton:
				this.SkeletonObject.SetActive(true);
			break;
			case Creature.Zombie:
				this.ZombieObject.SetActive(true);
			break;
			case Creature.Human:
				this.HumanObject.SetActive(true);
			break;
		}
		// small shake
		transform.Translate(Vector3.right * speed * Time.deltaTime);
		transform.Translate(Vector3.left * speed * Time.deltaTime);
	}

	public void Death()
	{
		MasterAudio.PlaySoundAndForget("respaw");
		if (GameConstants.humanUnlocked)
		{
			if (Checkpoint4) {
				SpawnText.SetActive(true); Invoke("HideSpawnText",1);
				transform.position= Checkpoint4.position;
				ChangeCharacter(Creature.Human);
				transform.localScale= new Vector3(1,1,1);
				InventoryUIManager.instance.GodVoice("Muerto?, ero no te preocupes\nSeras reanimado\n");
				return;
			}
		} 
		else if (GameConstants.zombieUnlocked)
		{
			if (Checkpoint3) {
				SpawnText.SetActive(true); Invoke("HideSpawnText",1);
				transform.position= Checkpoint3.position;
				ChangeCharacter(Creature.Zombie);
				transform.localScale= new Vector3(1,1,1);
				InventoryUIManager.instance.GodVoice("Muerto?, ero no te preocupes\nSeras reanimado\n");
				return;
			}
		}
		else if (GameConstants.skeletonUnlocked)
		{
			if (Checkpoint2) {
				SpawnText.SetActive(true); Invoke("HideSpawnText",1);
				transform.position= Checkpoint2.position;
				ChangeCharacter(Creature.Skeleton);
				transform.localScale= new Vector3(1,1,1);
				InventoryUIManager.instance.GodVoice("Muerto?, ero no te preocupes\nSeras reanimado\n");
				return;
			}
		}
		else
		{
			if (Checkpoint1) {
				SpawnText.SetActive(true); Invoke("HideSpawnText",1);
				transform.position= Checkpoint1.position;
				ChangeCharacter(Creature.Ghost);
				transform.localScale= new Vector3(1,1,1);
				InventoryUIManager.instance.GodVoice("Muerto?, ero no te preocupes\nSeras reanimado\n");
				return;
			}
		}
	}

	void HideSpawnText()
	{
		SpawnText.SetActive(false);
	}
}
