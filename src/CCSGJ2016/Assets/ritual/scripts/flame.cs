﻿using UnityEngine;
using System.Collections;

public class flame : MonoBehaviour {

	void Start()
	{
		Invoke("flip",1);
	}
	/*
	void OnTriggerEnter2D(Collider2D other) 
	{
		Debug.Log("enter");
		if (other.tag== "Player")
		{
			Debug.Log("player");
			if (GameConstants.currentPlayerType!= Creature.Skeleton) 	other.GetComponent<PlayerControl>().Death();

		}
	}
	*/

	void OnCollisionEnter2D(Collision2D  collision) 
	{
		Collider2D collider = collision.collider;
		
		if(collider.tag == "Player")
		{ 
			//Debug.Log(GameConstants.currentPlayerType);
			if (GameConstants.currentPlayerType== Creature.Ghost) 	this.GetComponent<BoxCollider2D>().isTrigger= false;
			else 													this.GetComponent<BoxCollider2D>().isTrigger= true;
			
			this.GetComponent<BoxCollider2D>().enabled= false;
			this.GetComponent<BoxCollider2D>().enabled= true;

			if (GameConstants.currentPlayerType!= Creature.Skeleton && GameConstants.currentPlayerType!= Creature.Ghost) 	collider.GetComponent<PlayerControl>().Death();
		}
		
	}
	
	void OnTriggerStay2D(Collider2D other) 
	{
		if (other.tag== "Player")
		{
			if (GameConstants.currentPlayerType== Creature.Ghost) 	this.GetComponent<BoxCollider2D>().isTrigger= false;
			else 													this.GetComponent<BoxCollider2D>().isTrigger= true;
			
			this.GetComponent<BoxCollider2D>().enabled= false;
			this.GetComponent<BoxCollider2D>().enabled= true;

			if (GameConstants.currentPlayerType!= Creature.Skeleton && GameConstants.currentPlayerType!= Creature.Ghost) 	other.GetComponent<PlayerControl>().Death();
		}
	}

	/*
	void OnTriggerExit2D(Collider2D other) 
	{
		if (other.tag== "Player")
		{
			if (GameConstants.currentPlayerType== Creature.Ghost) 	this.GetComponent<BoxCollider2D>().isTrigger= false;
			else 													this.GetComponent<BoxCollider2D>().isTrigger= true;
			
			this.GetComponent<BoxCollider2D>().enabled= false;
			this.GetComponent<BoxCollider2D>().enabled= true;

			if (GameConstants.currentPlayerType!= Creature.Skeleton) 	other.GetComponent<PlayerControl>().Death();
		}
	}*/

	void flip()
	{
		this.transform.localScale= new Vector3(-1f*this.transform.localScale.x,6f,6f);
		Invoke("flip",1);
	}

}
