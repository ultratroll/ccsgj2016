﻿using UnityEngine;
using System.Collections;

public class WalkAnimation : MonoBehaviour {
	
	Vector3 position;
	Animator animator;

	void Start()
	{
		position= transform.position;
		animator= this.GetComponent<Animator>();
	}

	// Update is called once per frame
	void Update () {
		if (position!= transform.position)
		{
			CancelInvoke("StopWalking");
			this.animator.SetBool("walking", true);
		} else
		{
			StopWalking();//Invoke("StopWalking",0.5f);
		}
		position= transform.position;
	}

	void StopWalking()
	{
		this.animator.SetBool("walking", false);
	}
}
