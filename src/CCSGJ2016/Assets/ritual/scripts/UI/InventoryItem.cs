﻿using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;

public class InventoryItem : MonoBehaviour {


	public ItemsCanCarry itemType;

	void OnTriggerStay2D(Collider2D other) {
		MasterAudio.PlaySoundAndForget("grabItem");
		InventoryUIManager.instance.AddItem(this.itemType);
		Destroy(this.gameObject);
	}

}
