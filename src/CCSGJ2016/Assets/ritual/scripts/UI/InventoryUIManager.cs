﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

[System.Serializable]
public class TextureByItem
{
	public ItemsCanCarry item;
	public Sprite sprite;
}

public class InventoryUIManager : MonoBehaviour {

	public Image[] cells;
	public List<ItemsCanCarry> items = new List<ItemsCanCarry>();

	public Text godVoice;

	public TextureByItem[] textures;

	public static InventoryUIManager instance;

	// Use this for initialization
	void Start () {
		instance= this;
		items = new List<ItemsCanCarry>();
	}
	
	public void Actualize()
	{
		for (int i=0;i<cells.Length;i++)
		{
			cells[i].sprite= null;
		}

		for (int i=0;i<items.Count;i++)
		{

			cells[i].sprite= GetTexture(items[i]);
		}


	}

	public void AddItem(ItemsCanCarry item)
	{
		items.Add(item);
		Actualize();
	}

	public bool RemoveItem(ItemsCanCarry item)
	{
		if (items.Contains(item)) {
			items.Remove(item);
			Actualize();
			return true;
		}
		return false;
	}

	public Sprite GetTexture(ItemsCanCarry item)
	{
		for (int i=0;i<textures.Length;i++)
		{
			if (textures[i].item == item)
				return textures[i].sprite;
		}
		return null;
	}

	public void GodVoice(string text)
	{
		godVoice.text+= text;
		Invoke("ClearGodVoice",3);
	}

	void ClearGodVoice()
	{
		godVoice.text= "";
	}
}
