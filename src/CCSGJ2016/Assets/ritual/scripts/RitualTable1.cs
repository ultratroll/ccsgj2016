﻿using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;

public class RitualTable1 : MonoBehaviour {

	public GameObject huesos;
	public GameObject tocadisco;
	public GameObject vela;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!GameConstants.skeletonUnlocked && huesos.activeSelf && tocadisco.activeSelf && vela.activeSelf)
		{
			GameConstants.skeletonUnlocked= true;
			Invoke("RitualSuccessMsg",4);
		}
	}

	public void CheckForItems()
	{
		if (InventoryUIManager.instance.RemoveItem(ItemsCanCarry.Huesos)) {
			MasterAudio.PlaySoundAndForget("objectAltar");
			huesos.SetActive(true);
			InventoryUIManager.instance.GodVoice("Huesos agregados al ritual!\n");
		}

		if (InventoryUIManager.instance.RemoveItem(ItemsCanCarry.Tocadisco)) {
			MasterAudio.PlaySoundAndForget("objectAltar");
			tocadisco.SetActive(true);
			InventoryUIManager.instance.GodVoice("Tocadiscos agregado al ritual!\n");
		}

		if (InventoryUIManager.instance.RemoveItem(ItemsCanCarry.Vela)) {
			MasterAudio.PlaySoundAndForget("objectAltar");
			vela.SetActive(true);
			InventoryUIManager.instance.GodVoice("Vela agregada al ritual!\n");
		}
	}

	void RitualSuccessMsg()
	{
		MasterAudio.PlaySoundAndForget("ritualComplete");
		StartCoroutine("FTUX");
		this.enabled= false;
	}

	IEnumerator FTUX()
	{
		yield return new WaitForSeconds(1f);
		InventoryUIManager.instance.GodVoice ("El ritual es un exito.\nForma de esqueleto disponible.\n");
		yield return new WaitForSeconds(3.5f);
		InventoryUIManager.instance.GodVoice("Presiona 'Q2' para cambiar de forma.\n");
		yield return null;
	}


}
