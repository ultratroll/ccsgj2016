﻿using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;

public class RitualTable2 : MonoBehaviour {

	public GameObject Carne;
	public GameObject Cuerda;
	public GameObject Sangre;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!GameConstants.zombieUnlocked && Carne.activeSelf && Cuerda.activeSelf && Sangre.activeSelf)
		{
			GameConstants.zombieUnlocked= true;
			Invoke("RitualSuccessMsg",4);
		}
	}

	public void CheckForItems()
	{
		if (InventoryUIManager.instance.RemoveItem(ItemsCanCarry.Carne)) {
			MasterAudio.PlaySoundAndForget("objectAltar");
			InventoryUIManager.instance.GodVoice("Carne agregada al ritual!\n");
			Carne.SetActive(true);
		}
		
		if (InventoryUIManager.instance.RemoveItem(ItemsCanCarry.Cuerda)) {
			MasterAudio.PlaySoundAndForget("objectAltar");
			InventoryUIManager.instance.GodVoice("Cuerda agregada al ritual!\n");
			Cuerda.SetActive(true);
		}
		
		if (InventoryUIManager.instance.RemoveItem(ItemsCanCarry.Sangre)) {
			MasterAudio.PlaySoundAndForget("objectAltar");
			InventoryUIManager.instance.GodVoice("Sangre agregada al ritual!\n");
			Sangre.SetActive(true);
		}
	}

	void RitualSuccessMsg()
	{
		MasterAudio.PlaySoundAndForget("ritualComplete");
		InventoryUIManager.instance.GodVoice("El ritual es un exito.\nForma de Zombie disponible.\nPresiona Q para cambiar de forma.\n");
		this.enabled= false;
	}
}
