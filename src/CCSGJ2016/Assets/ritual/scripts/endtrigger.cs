﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class endtrigger : MonoBehaviour {

	public float speed= 100f;

	public Image curtain;

	void Start()
	{
		//StartCoroutine("EndGame");
	}

	void OnTriggerEnter2D(Collider2D other) 
	{
		if (other.tag== "Player")
		{
			if (GameConstants.currentPlayerType== Creature.Human) 	{
				StartCoroutine("EndGame");
			}
		}
	}

	IEnumerator EndGame() {

		yield return new WaitForSeconds(0.5f);
		Color c = curtain.color;
		c.a = 0.2f;
		curtain.color = c;

		yield return new WaitForSeconds(0.5f);
		c = curtain.color;
		c.a = 0.4f;
		curtain.color = c;

		yield return new WaitForSeconds(0.5f);
		c = curtain.color;
		c.a = 0.6f;
		curtain.color = c;

		yield return new WaitForSeconds(0.5f);
		c = curtain.color;
		c.a = 0.8f;
		curtain.color = c;

		yield return new WaitForSeconds(0.5f);
		c = curtain.color;
		c.a = 1.0f;
		curtain.color = c;

		Application.LoadLevel("ending1");
		yield return null;
	}

	/*
	void OnCollisionEnter2D(Collision2D  collision) 
	{
		Collider2D collider = collision.collider;
		
		if(collider.tag == "Player")
		{ 
			//Debug.Log(GameConstants.currentPlayerType);
			if (GameConstants.currentPlayerType== Creature.Ghost) 	this.GetComponent<BoxCollider2D>().isTrigger= true;
			else 													this.GetComponent<BoxCollider2D>().isTrigger= false;

			this.GetComponent<BoxCollider2D>().enabled= false;
			this.GetComponent<BoxCollider2D>().enabled= true;
		}

	}

	void OnTriggerStay2D(Collider2D other) 
	{
		if (other.tag== "Player")
		{
			if (GameConstants.currentPlayerType== Creature.Ghost) 	this.GetComponent<BoxCollider2D>().isTrigger= true;
			else 													this.GetComponent<BoxCollider2D>().isTrigger= false;

			this.GetComponent<BoxCollider2D>().enabled= false;
			this.GetComponent<BoxCollider2D>().enabled= true;
		}
	}
	
	void OnTriggerExit2D(Collider2D other) 
	{
		if (other.tag== "Player")
		{
			if (GameConstants.currentPlayerType== Creature.Ghost) 	this.GetComponent<BoxCollider2D>().isTrigger= true;
			else 													this.GetComponent<BoxCollider2D>().isTrigger= false;

			this.GetComponent<BoxCollider2D>().enabled= false;
			this.GetComponent<BoxCollider2D>().enabled= true;
		}
	}
*/

}
