﻿using UnityEngine;
using System.Collections;

public enum Creature
{
	Ghost,
	Skeleton,
	Zombie,
	Human
}

public enum ItemsCanCarry
{
	None,
	Huesos,
	Tocadisco,
	Vela,
	Carne,
	Sangre,
	Cuerda,
	Cerebro,
	Corazon,
	Cafe,
	Desodorante,
	RadioRota,
	BrainPasta,
	HuesoSexy,
	Guitar


}

public class GameConstants : MonoBehaviour {

	//
	public static Creature currentPlayerType= Creature.Ghost;

	public static bool skeletonUnlocked= false;
	public static bool zombieUnlocked= false;
	public static bool humanUnlocked= false;


}
