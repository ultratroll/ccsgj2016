﻿using UnityEngine;
using System.Collections;
using DarkTonic.MasterAudio;

public class RitualTable3 : MonoBehaviour {

	public GameObject Cafe;
	public GameObject Cerebro;
	public GameObject Corazon;
	public GameObject Desorodante;
	public GameObject Soldado;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!GameConstants.humanUnlocked && Cafe.activeSelf && Cerebro.activeSelf && Corazon.activeSelf && Desorodante.activeSelf)
		{
			GameConstants.humanUnlocked= true;
			Invoke("RitualSuccessMsg",4);
		}
	}

	public void CheckForItems()
	{
		if (InventoryUIManager.instance.RemoveItem(ItemsCanCarry.Cafe)){
			MasterAudio.PlaySoundAndForget("objectAltar");
			InventoryUIManager.instance.GodVoice("Cafe agregado al ritual!\n");
			Cafe.SetActive(true);
		}
		if (InventoryUIManager.instance.RemoveItem(ItemsCanCarry.Cerebro)){
			MasterAudio.PlaySoundAndForget("objectAltar");
			InventoryUIManager.instance.GodVoice("Cerebro agregado al ritual!\n");
			Cerebro.SetActive(true);
		}
		if (InventoryUIManager.instance.RemoveItem(ItemsCanCarry.Corazon)){
			MasterAudio.PlaySoundAndForget("objectAltar");
			InventoryUIManager.instance.GodVoice("Corazon agregado al ritual!\n");
			Corazon.SetActive(true);
		}
		if (InventoryUIManager.instance.RemoveItem(ItemsCanCarry.Desodorante)){
			MasterAudio.PlaySoundAndForget("objectAltar");
			InventoryUIManager.instance.GodVoice("Desorodante agregado al ritual!\n");
			Desorodante.SetActive(true);
		}
	}

	void RitualSuccessMsg()
	{
		MasterAudio.PlaySoundAndForget("ritualComplete");
		InventoryUIManager.instance.GodVoice("El ritual es un exito.\nForma de humano disponible.\nPresiona Q para cambiar de forma.\n");
		this.enabled= false;
		Destroy (this.Soldado);
	}
}
