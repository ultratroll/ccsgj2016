﻿using UnityEngine;
using System.Collections;

public class movablebrick : MonoBehaviour {

	public float speed= 100f;
	
	void OnCollisionEnter2D(Collision2D  collision) 
	{
		Collider2D collider = collision.collider;
		
		if(collider.tag == "Player")
		{ 
			//Debug.Log(GameConstants.currentPlayerType);
			if (GameConstants.currentPlayerType== Creature.Zombie) 	GetComponent<Rigidbody2D>().isKinematic = false;
			else 													GetComponent<Rigidbody2D>().isKinematic = true;
			this.GetComponent<BoxCollider2D>().enabled= false;
			this.GetComponent<BoxCollider2D>().enabled= true;
		}
	}
}
