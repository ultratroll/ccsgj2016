﻿using UnityEngine;
using System.Collections;

public class NPCControlMusico : MonoBehaviour {

	public float speed= 4f;
	public Creature creatureType;

	public GameObject GhostText;
	public GameObject SkeletonText;
	public GameObject ZombieText;
	public GameObject HumanText;

	public GameObject gift;

	public void PlayerNear()
	{
		ChangeText();
	}

	public void PlayerAway()
	{
		this.GhostText.SetActive(false);
		this.SkeletonText.SetActive(false);
		this.ZombieText.SetActive(false);
		this.HumanText.SetActive(false);
	}

	public void ChangeText()
	{
		TryTake();

		this.GhostText.SetActive(false);
		this.SkeletonText.SetActive(false);
		this.ZombieText.SetActive(false);
		this.HumanText.SetActive(false);

		switch(GameConstants.currentPlayerType)
		{
		case Creature.Ghost:
			this.GhostText.SetActive(true);
			break;
		case Creature.Skeleton:
			this.SkeletonText.SetActive(true);
			break;
		case Creature.Zombie:
			this.ZombieText.SetActive(true);
			break;
		case Creature.Human:
			this.HumanText.SetActive(true);
			break;
		}
	}

	public void TryTake()
	{

		if (InventoryUIManager.instance.RemoveItem(ItemsCanCarry.Guitar)) {
			gift.SetActive(true);

			this.GhostText.GetComponent<TextMesh>().text= "CLAAAA CLAAAAA";
			this.SkeletonText.GetComponent<TextMesh>().text= "Someday I'll wish \n upon a star And wake up \n where the clouds \n are far Behind me";
			this.ZombieText.GetComponent<TextMesh>().text= "CLAAAA CLAAAAA!";
			this.HumanText.GetComponent<TextMesh>().text= "CLAAAA CLAAAAA";


			InventoryUIManager.instance.GodVoice("Te han regalado un Hueso Sexy!\n");
		}


	}
}
